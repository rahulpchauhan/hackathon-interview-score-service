package com.tenup.hackathon.interviewscore.config;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@Deprecated
public class OpenAIWebClientConfig {
	
	@Deprecated
	public WebClient openAIWebClient(MediaType mediaType) {
		return WebClient.builder()
				.exchangeStrategies(ExchangeStrategies.builder()
						.codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024)).build())
				.defaultHeaders(httpHeaders -> {
					httpHeaders.add(HttpHeaders.CONTENT_TYPE, mediaType.toString());
					httpHeaders.add(HttpHeaders.AUTHORIZATION,
							"Bearer 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");
				}).baseUrl("https://api.openai.com").build();
	}
}
