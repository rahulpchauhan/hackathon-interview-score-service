package com.tenup.hackathon.interviewscore.service.implementation;

import java.io.InputStream;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import com.tenup.hackathon.interviewscore.dto.ChatCompletion;
import com.tenup.hackathon.interviewscore.dto.ChatCompletionReply;
import com.tenup.hackathon.interviewscore.dto.Message;
import com.tenup.hackathon.interviewscore.dto.Query;
import com.tenup.hackathon.interviewscore.dto.Response;
import com.tenup.hackathon.interviewscore.service.AIService;

import reactor.core.publisher.Mono;

@Service
public class OpenAIService implements AIService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	protected String role = "user";
	@Value("${app.openai.model.chat}")
	protected String modelChat;
	@Value("${app.openai.model.audio}")
	protected String modelAudio;

	@Value("${app.openai.key}")
	protected String openAIKey;
	@Value("${app.openai.baseUrl}")
	protected String baseUrl;
	@Value("app.openai.uri")
	protected String uri;

	@Override
	public Response createChatCompletion(Query query) {
		Objects.requireNonNull(query);
		var messages = query.getQuestions().stream().map(q -> Message.builder().content(q).role(role).build())
				.collect(Collectors.toList());

		var chatCompletion = ChatCompletion.builder().messages(messages).model(modelChat).build();

		ChatCompletionReply response = null;
		try {
			response = WebClient.builder()
					.exchangeStrategies(ExchangeStrategies.builder()
							.codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024)).build())
					.defaultHeaders(httpHeaders -> {
						httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
						httpHeaders.add(HttpHeaders.AUTHORIZATION, openAIKey);
					})
					.baseUrl(baseUrl)
					.build()
					.post()
					.uri(uri)
					.body(Mono.just(chatCompletion), ChatCompletion.class)
					.accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(ChatCompletionReply.class).block();
		} catch (Exception e) {
			logger.error("Erroe while creating chat completion: ", e.getMessage());
		}

		logger.info("OpenAIService:createChatCompletion: response: {}", response);

		return Response.builder().responses(response.getChoices().stream()
				.map(choice -> choice.getMessage().getContent()).collect(Collectors.toList())).build();
	}
	
	public Response createTranscription(Query query) {
		
		var file = getClass().getResourceAsStream("/audio/audio.mp3");
		MultipartBodyBuilder builder = new MultipartBodyBuilder();
		builder.part("file", file);
		builder.part("model", "whisper-1");
		
		var response = WebClient.builder()
				.exchangeStrategies(ExchangeStrategies.builder()
						.codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024)).build())
				.defaultHeaders(httpHeaders -> {
					httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
					httpHeaders.add(HttpHeaders.AUTHORIZATION, openAIKey);
				})
			    .baseUrl(baseUrl)
			    .build()
			    .post()
			    .contentType(MediaType.MULTIPART_FORM_DATA)
			    .body(BodyInserters.fromMultipartData(builder.build()))
			    .accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(ChatCompletionReply.class).block();
//			    .exchangeToMono(response -> {
//			        if (response.statusCode().equals(HttpStatus.OK)) {
//			            return response.bodyToMono(HttpStatus.class).thenReturn(response.statusCode());
//			        } else {
//			            throw new ServiceException("Error uploading file");
//			        }
//			      });
		return null;
	}

}
