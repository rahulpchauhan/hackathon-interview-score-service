package com.tenup.hackathon.interviewscore.service;

import com.tenup.hackathon.interviewscore.dto.Query;
import com.tenup.hackathon.interviewscore.dto.Response;

public interface AIService {
	
	public Response createChatCompletion(Query query);	

}
