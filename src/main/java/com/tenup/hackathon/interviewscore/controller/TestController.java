package com.tenup.hackathon.interviewscore.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tenup.hackathon.interviewscore.dto.Query;
import com.tenup.hackathon.interviewscore.dto.Request;
import com.tenup.hackathon.interviewscore.dto.Response;
import com.tenup.hackathon.interviewscore.service.AIService;

@RestController
@RequestMapping("/score/api/v1")
public class TestController {
	
	private AIService aiService;
	
	public TestController(AIService aiService) {
		this.aiService = aiService;
	}
	
	@PostMapping(value = "/ask")
	public ResponseEntity<Response> test(@RequestBody Request question) {
		var response = aiService.createChatCompletion(Query.builder().questions(question.getQuestion()).build());
		return ResponseEntity.ok(response);
	}
}
