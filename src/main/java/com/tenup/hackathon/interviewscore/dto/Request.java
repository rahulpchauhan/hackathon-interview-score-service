package com.tenup.hackathon.interviewscore.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Request implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7109324919612434310L;
	protected List<String> question;
}
