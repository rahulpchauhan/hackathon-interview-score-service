package com.tenup.hackathon.interviewscore.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Query implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4713785160759017779L;
	protected List<String> questions;

	
	
}
