package com.tenup.hackathon.interviewscore.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ChatCompletionReply implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4391332410893007783L;
	protected String id;
	protected String object;
	protected long created;
	protected String model;
	protected List<Choice> choices;
}
