package com.tenup.hackathon.interviewscore.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class Message implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5094100934638454434L;
	protected String role;
	protected String content;

}
