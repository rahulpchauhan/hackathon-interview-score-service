package com.tenup.hackathon.interviewscore.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class Response implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3757700790695631814L;
	protected List<String> responses;	
}
