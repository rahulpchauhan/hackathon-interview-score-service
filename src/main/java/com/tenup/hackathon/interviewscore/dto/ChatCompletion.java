package com.tenup.hackathon.interviewscore.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class ChatCompletion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8116086372748882247L;
	protected String model;
	protected List<Message> messages;	
}
