package com.tenup.hackathon.interviewscore.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class Choice implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4864868280156803760L;
	protected Message message;
	@JsonProperty(value = "finish_reason")
	protected String finishReason;
	
	
}
